package golangtest

import (
    "appengine"
    "appengine/datastore"
    "appengine/user"
    "http"
    "template"
    "strings"
    "time"
)

type Greeting struct {
    Body    string
    AccountId string
    AccountEmail string
    Date    datastore.Time
}

var timeZone = map[string] int64 {
    "UTC":  0*60*60,
    "EST": -5*60*60,
    "CST": -6*60*60,
    "MST": -7*60*60,
    "PST": -8*60*60,
    "JST": 9*60*60,
}

/*
 * Take a datastore time (microseconds since the epoch)
 * and return a time.Time pointer converted to JST
 */
func TimeToTime(t datastore.Time) *time.Time {
    // Use SecondsToUTC but since we already offset
    // the time it's ok.
    return time.SecondsToUTC((int64(t)/1000000) + timeZone["JST"])
}

func userNameFormatter(data ...interface{}) string {
    if (len(data) == 1) {
        s, _ := data[0].(string)
        splits := strings.SplitN(s, "@", 2)
        return splits[0][0:len(splits[0])/2] + "...@" + splits[1]
    }
    return ""
}

func dateFormatter(data ...interface{}) string {
    if (len(data) == 1) {
        date, _ := data[0].(datastore.Time)
        return TimeToTime(date).Format("2006-01-02 15:04:05")
    }
    return ""
}

func newlineFormatter(data ...interface{}) string {
    if (len(data) == 1) {
        b, _ := data[0].(string)
        return strings.Replace(b, "\n", "<br/>", -1)
    }
    return ""
}

func init() {
    http.HandleFunc("/", func(w http.ResponseWriter, request *http.Request) {
        c := appengine.NewContext(request)
        // TODO: Cache templates
        t := template.New("Guestbook template")
        t = t.Funcs(template.FuncMap{
            "date": dateFormatter,
            "userName": userNameFormatter,
            "newlinesbr": newlineFormatter,
        })
        t, _ = t.ParseFile("templates/base.html")

        current_user := user.Current(c)
        var login_url string
        if current_user == nil {
            login_url, _ = user.LoginURL(c, "/")
        } else {
            login_url, _ = user.LogoutURL(c, "/")
        }

        greetings := &[]Greeting{}
        datastore.NewQuery("Greeting").
                  Order("-Date").
                  GetAll(c, greetings)

        w.Header().Set("Content-Type", "text/html")
        err := t.Execute(w, struct{
             CurrentUser *user.User
             Greetings *[]Greeting
             LoginUrl string
        }{
            CurrentUser: current_user,
            Greetings: greetings,
            LoginUrl: login_url,
        })
        if err != nil {
            //Not Safe
            http.Error(w, err.String(), http.StatusInternalServerError)
        }
    })

    http.HandleFunc("/save", func(w http.ResponseWriter, request *http.Request) {
        c := appengine.NewContext(request)
        current_user := user.Current(c)
        if current_user == nil {
            login_url, _ := user.LoginURL(c, "/")
            http.Redirect(w, request, login_url, http.StatusFound)
            return
        }

        body := request.FormValue("body")
        if (len(body) > 0) {
            g := &Greeting{
                Body: body,
                AccountId: current_user.Id,
                AccountEmail: current_user.Email,
                Date: datastore.SecondsToTime(time.Seconds()),
            }
            datastore.Put(c, datastore.NewIncompleteKey(c, "Greeting", nil), g)
        }
        http.Redirect(w, request, "/", http.StatusFound)
    })
}
